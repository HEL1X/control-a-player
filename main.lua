-- CREDS ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

--[[               ____  ______| dP     dP   88888888b dP        dP dP    dP 
 _      __(_)___  / __ \/ ____/| 88     88   88        88        88 Y8.  .8P 
| | /| / / / __ \/ /_/ /___ \  | 88aaaaa88a a88aaaa    88        88  Y8aa8P  
| |/ |/ / / / / /\__, /___/ /  | 88     88   88        88        88 d8'  `8b
|__/|__/_/_/ /_//____/_____/   | 88     88   88        88        88 88    88 
                               | dP     dP   88888888P 88888888P dP dP    dP 
]]--
------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-- LOAD STATEMENT -------------------

print("control-a-player loaded!")

-------------------------------------

-- VARS ------------------------------

local player = game.Players.LocalPlayer
local mainacctName = "" -- add main account name here
if mainacctName == "" then
    mainacctName = player.Name
end
local Character = player.Character
local Humanoid = Character:FindFirstChild("Humanoid")
local HumRoot = Character:WaitForChild("HumanoidRootPart")
local ReplicatedStorage = game:GetService("ReplicatedStorage")

--------------------------------------

-- CHANGENAME FUNCTION ------------------------

function changeName(n)

    local A_1 = "Update"
    
    local A_2 = {
      ["DescriptionText"] = n,
      ["ImageId"] = "@#!$%!@$!@%!/@$@%!@$@%"
    }
    local event = ReplicatedStorage.CustomiseBooth
  
    event:FireServer(A_1, A_2)
  
  end
  
---------------------------------------------------

function chat(st)

local args = {
    [1] = st,
    [2] = "All"
}

game:GetService("ReplicatedStorage").DefaultChatSystemChatEvents.SayMessageRequest:FireServer(unpack(args))
end



i=0
posit=nil
msgWithPlr = "Control a player!\n Controls: wasd (lowercased, in chat), jump, or 'turn' to turn 90 degrees. \n Last player to control: "
changeName("Control a player!\n Controls: wasd (lowercased, in chat), jump, or 'turn' to turn 90 degrees.")
dist=2
game:GetService("Players").PlayerChatted:Connect(function(_,plr, message)
    if i%2 ==0 then
    if message == "w" then
        HumRoot.CFrame = HumRoot.CFrame*CFrame.new(0,0,-dist)
        changeName(msgWithPlr .. plr.Name)
    end
    if message == "s" then
        HumRoot.CFrame = HumRoot.CFrame*CFrame.new(0,0,dist)
        changeName(msgWithPlr .. plr.Name)
    end
    if message == "a" then
        HumRoot.CFrame = HumRoot.CFrame*CFrame.new(dist,0,0)
        changeName(msgWithPlr .. plr.Name)
    end
    if message == "d" then
        HumRoot.CFrame = HumRoot.CFrame*CFrame.new(-dist,0,0)
        changeName(msgWithPlr .. plr.Name)
    end
    if message == "turn" then
        local b = -90
        local newFrame = CFrame.Angles(0, math.rad(b), 0)
        HumRoot.CFrame = HumRoot.CFrame*newFrame
        changeName(msgWithPlr .. plr.Name)
    end
    if message == "render" then
        changeName("Control a player!\n Controls: wasd (lowercased, in chat), jump, or 'turn' to turn 90 degrees.")
    end
    if message == "jump" then
        Humanoid:ChangeState(Enum.HumanoidStateType.Jumping)
        changeName(msgWithPlr .. plr.Name)
    end
end
if message:sub(0,7) == "!speed " and plr.Name == mainacctName then
    dist = tonumber(message:sub(7,message:len()))
end
if message == "set pos" and plr.Name == mainacctName then
    posit=HumRoot.Position
    chat("Set position")
end
if message == "return pos" and plr.Name == mainacctName then
    chat("Returning to set position")
    HumRoot.CFrame = CFrame.new(posit)
end
i+=1
end)